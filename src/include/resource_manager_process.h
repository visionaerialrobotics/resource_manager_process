/*!*********************************************************************************
 *  \file       resource_manager_process.h
 *  \brief      ResourceManager definition file.
 *  \details    This file contains the ResourceManager declaration.
 *              To obtain more information about it's definition consult
 *              the resource_manager_process.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef RESOURCE_MANAGER_PROCESS_H
#define RESOURCE_MANAGER_PROCESS_H

#include <map>
#include <string>
#include <tuple>

#include <ros/ros.h>
#include "std_srvs/Empty.h"

#include <droneMsgsROS/RequestResources.h>
#include <droneMsgsROS/QueryResources.h>
#include <droneMsgsROS/ListOfCapabilities.h>
#include <aerostack_msgs/ListOfProcesses.h>
#include <droneMsgsROS/CheckCapabilitiesConsistency.h>

#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/askForModule.h>
#include <droneMsgsROS/setControlMode.h>

#include <drone_process.h>
#include "capability.h"

class ResourceManager : public DroneProcess
{
private:
  std::map<std::string, Capability> active_capabilities;
  ros::NodeHandle node_handle;

  std_srvs::Empty empty_msg;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;
  std::string query_resources_str;
  std::string check_capabilities_consistency_str;
  std::string activate_resources_str;
  std::string cancel_resources_str;
  std::string list_of_active_capabilities_str;
  std::string list_of_active_processes_str;

  std::string estimated_pose_str;
  std::string reference_position_str;
  std::string yaw_controller_str;
  std::string service_topic_str;

  ros::ServiceClient query_resources_cli;
  ros::ServiceClient check_capabilities_consistency_cli;
  ros::ServiceServer activate_resources_srv;
  ros::ServiceServer cancel_resources_srv;
  ros::Publisher list_of_active_capabilities_topic;
  ros::Publisher list_of_active_processes_topic;

public:
  ResourceManager();
  ~ResourceManager();

private://DroneProcess
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();

private:
  std::tuple<bool,std::vector<std::string> > checkConsistencies(std::vector<Capability>);
  std::tuple<bool, std::string, std::vector<Capability> > queryResources(std::string behavior_name);
  std::tuple<bool, std::string> startProcesses(std::vector<Capability>);
  std::tuple<bool, std::string> startProcesses(Capability);
  std::tuple<bool, std::string> stopProcesses(std::vector<Capability>);
  std::tuple<bool, std::string> stopProcesses(Capability);
  std::tuple<bool, std::string> stopUnreferenceCapabilities();
  void updateActiveResources();

public://Callbacks
  bool activateCapabilitiesCallback(droneMsgsROS::RequestResources::Request&, droneMsgsROS::RequestResources::Response&);
  bool cancelCapabilitiesCallback(droneMsgsROS::RequestResources::Request&, droneMsgsROS::RequestResources::Response&);
};

#endif
