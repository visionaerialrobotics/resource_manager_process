/*!*******************************************************************************************
 *  \file       resource_manager_process.cpp
 *  \brief      ResourceManager implementation file.
 *  \details    This file implements the ResourceManager class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/resource_manager_process.h"

ResourceManager::ResourceManager() : DroneProcess()
{

}

ResourceManager::~ResourceManager()
{

}


/*functions of DroneProcess*/
void ResourceManager::ownSetUp()
{

  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
				 "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  private_nh.param<std::string>("behavior_resources_cli", query_resources_str, "behavior_resources");
  private_nh.param<std::string>("activate_resources_srv", activate_resources_str, "activate_resources");
  private_nh.param<std::string>("cancel_resources_srv", cancel_resources_str, "cancel_resources");
  private_nh.param<std::string>("list_of_active_capabilities_topic", list_of_active_capabilities_str, "list_of_active_capabilities");
  private_nh.param<std::string>("list_of_active_processes_topic", list_of_active_processes_str, "list_of_active_processes");
  private_nh.param<std::string>("check_capabilities_consistency_srv", check_capabilities_consistency_str, "check_capability_consistency");
}


void ResourceManager::ownStart()
{
  activate_resources_srv = node_handle.advertiseService(activate_resources_str, &ResourceManager::activateCapabilitiesCallback, this);
  cancel_resources_srv = node_handle.advertiseService(cancel_resources_str, &ResourceManager::cancelCapabilitiesCallback, this);
  query_resources_cli = node_handle.serviceClient<droneMsgsROS::QueryResources>(query_resources_str);
  check_capabilities_consistency_cli = node_handle.serviceClient<droneMsgsROS::CheckCapabilitiesConsistency>(check_capabilities_consistency_str);
  list_of_active_capabilities_topic = node_handle.advertise<droneMsgsROS::ListOfCapabilities>(list_of_active_capabilities_str, 1000);
  list_of_active_processes_topic = node_handle.advertise<aerostack_msgs::ListOfProcesses>(list_of_active_processes_str, 1000);
}


void ResourceManager::ownRun()
{

}


void ResourceManager::ownStop()
{
  query_resources_cli.shutdown();
  activate_resources_srv.shutdown();
  cancel_resources_srv.shutdown();
  list_of_active_capabilities_topic.shutdown();
  list_of_active_processes_topic.shutdown();
}


/*Callbacks*/
bool ResourceManager::activateCapabilitiesCallback(droneMsgsROS::RequestResources::Request& request,
						   droneMsgsROS::RequestResources::Response& response)
{
  std::cout << "[INFO]: Activating capabilities for: [ "
            << request.behavior_name << " ] " << std::endl;

  /*Getting capability data*/
  bool query_result;
  std::string error_message;
  std::vector<Capability> capabilities_to_activate;
  std::tie(query_result, error_message, capabilities_to_activate) =
    queryResources(request.behavior_name);

  if(!query_result){
    response.acknowledge = false;
    response.error_message = error_message;
    std::cout << "[ERROR]: " << error_message
              << std::endl;
    return true;
  }

  /*Return earlier if no capabilities are needed to be activated */
  if(capabilities_to_activate.empty()){
    std::cout << "No need to activate capabilities for behavior ["<<request.behavior_name<<"] "
              << std::endl;

    std::string error_msg; bool stopped;
    std::tie(stopped, error_msg) = stopUnreferenceCapabilities();
    if(!stopped)
      std::cout << error_msg << std::endl;

    response.acknowledge = true;
    return true;
  }

  /*Detect if any capability is already active*/
  for(auto& capability_pair: active_capabilities) {
    if(std::find(capabilities_to_activate.begin(),
                capabilities_to_activate.end(),
                capability_pair.second)
       != capabilities_to_activate.end()) {
      std::cout <<"["<<request.behavior_name<<"]: "<<"Capability: " << capability_pair.second.getName()
                << " is already active. Incrementing reference number " << std::endl;

      capability_pair.second.incrementReferences();
      capabilities_to_activate.erase(std::remove(capabilities_to_activate.begin(),
                                                 capabilities_to_activate.end(),
                                                 capability_pair.second),
                                     capabilities_to_activate.end());
      continue;
    }
  }
  if(capabilities_to_activate.empty()){
    std::cout << "Every capability is already active. " << std::endl;
    bool stopped;
    std::tie(stopped, error_message) = stopUnreferenceCapabilities();

    response.acknowledge = true;
    response.error_message = "";
    if(!stopped){
      response.acknowledge = false;
      response.error_message = error_message;
    }
    return true;
  }

  /*Check consistency between capabilities */
  droneMsgsROS::CheckCapabilitiesConsistency check_capabilities_consistency_msg;
  std::for_each(capabilities_to_activate.begin(), capabilities_to_activate.end(),
                [&](Capability capability){
                  check_capabilities_consistency_msg.request.capabilities_to_activate
                    .push_back(capability.getName());
                });
  std::for_each(active_capabilities.begin(), active_capabilities.end(),
		[&](std::pair<std::string, Capability> pair_name_capability){
                  if(pair_name_capability.second.getReferenceNumber() > 0){
                    check_capabilities_consistency_msg.request.active_capabilities
                      .push_back(pair_name_capability.first);
                  }else{
                    std::cout << "Capability ["<<pair_name_capability.first<<"] has reference 0. It was not send to the behaviorSpecialist "
                              << std::endl;
                  }
		});

  check_capabilities_consistency_cli.call(check_capabilities_consistency_msg);

  if(!check_capabilities_consistency_msg.response.consistent){
    std::cout << "Behavior ["<<request.behavior_name<<"]: "
              << "Found inconsistencies with capabilities " << std::endl;
    response.acknowledge = false;
    response.error_message = check_capabilities_consistency_msg.response.error_message;

    std::string error_msg; bool stopped;
    std::tie(stopped, error_msg) = stopUnreferenceCapabilities();
    if(!stopped)
      std::cout << error_msg << std::endl;

    return true;
  }

  /*Activate Capabilities*/
  bool activated;
  std::tie(activated, error_message) = startProcesses(capabilities_to_activate);
  if(!activated){
    response.acknowledge = false;
    response.error_message = error_message;

    std::string error_msg; bool stopped;
    std::tie(stopped, error_msg) = stopUnreferenceCapabilities();
    if(!stopped)
      std::cout << error_msg << std::endl;

    return true;
  }

  /*Update references and save Capabilities in memory*/
  for(auto capability : capabilities_to_activate){
    capability.incrementReferences();
    active_capabilities.insert({capability.getName(),capability});
  }

  std::string error_msg; bool stopped;
  std::tie(stopped, error_msg) = stopUnreferenceCapabilities();
  if(!stopped)
    std::cout << error_msg << std::endl;

  response.acknowledge = true;
  updateActiveResources();   /* Update other processes with active capabilities and processes */
  return true;
}


bool ResourceManager::cancelCapabilitiesCallback(droneMsgsROS::RequestResources::Request& request,
						 droneMsgsROS::RequestResources::Response& response)
{
  /* Getting capability data */
  bool query_result;
  std::string error_message;
  std::vector<Capability> capabilities_to_deactivate;
  std::tie(query_result, error_message, capabilities_to_deactivate) =
    queryResources(request.behavior_name);

  if(!query_result){
    response.acknowledge = false;
    response.error_message = error_message;
    return true;
  }

  /* Checks if some capability is not active */
  for(auto capability : capabilities_to_deactivate){
    if(!active_capabilities.count(capability.getName())){
       response.acknowledge = false;
       response.error_message = "Capability ["+capability.getName()+"]"+
         " was not actived when trying to cancel it ";
       std::cout << response.error_message << std::endl;
      return true;
    }
  }

  for(auto capability : capabilities_to_deactivate){
    std::cout << "Decrementing reference number for capability: [ " << capability.getName() << " ] " << std::endl;
    active_capabilities.find(capability.getName())->second.decrementReferences();
  }
  response.acknowledge = true;
  response.error_message = "";
  return true;
}


/*Private functions*/
std::tuple<bool, std::string, std::vector<Capability> >
                ResourceManager::queryResources(std::string behavior_name)
{
  droneMsgsROS::QueryResources query_msg;
  query_msg.request.behavior_name = behavior_name;

  query_resources_cli.call(query_msg);
  if(!query_msg.response.found){
    std::vector<Capability> empty_vector;
    return std::make_tuple(false, query_msg.response.error_message, empty_vector);
  }

  std::vector<Capability> capabilities_to_active;
  for(auto capability : query_msg.response.capabilities){
    capabilities_to_active.push_back(Capability(capability.name,
						capability.processes));
  }
  return std::make_tuple(true, "", capabilities_to_active);
}


std::tuple<bool, std::string>
      ResourceManager::startProcesses(std::vector<Capability> capabilities_to_activate)
{
  bool everything_activated = true;
  bool activated;
  std::string error_message = "The following processes had an error: \n";
  std::string error_message_process;
  for(auto capability : capabilities_to_activate){
    std::tie(activated, error_message_process) = startProcesses(capability);
    if(!activated){
      everything_activated = false;
      error_message += error_message_process+"\n";
    }
  }
  return std::make_tuple(everything_activated, error_message);
}


std::tuple<bool, std::string>
       ResourceManager::startProcesses(Capability capability)
{
  for(auto process: capability.getProcesses())
  {
    /*Starting process*/
    std::string process_path = "/"+drone_id_namespace+"/"+process+"/start";

    ros::ServiceClient process_cli = node_handle.serviceClient<std_srvs::Empty>(process_path);
    if(!process_cli.call(empty_msg)){
      std::cout << "[ERROR]: Process " << process << " couldn't be started " << std::endl;
      return std::make_tuple(false, "Process ["+process+"] could not be started ");
    }

    std::cout << "[INFO]: Process " << process << " is activated" << std::endl;
  }

  return std::make_tuple(true, "");
}


std::tuple<bool, std::string>
          ResourceManager::stopProcesses(std::vector<Capability> capabilities_to_deactivate)
{
  bool everything_deactivated = true;
  std::string error_message = "The following processes had an error: \n";
  for(auto capability : capabilities_to_deactivate){
    bool stopped; std::string error_message_process;
    std::tie(stopped, error_message_process) = stopProcesses(capability);
    if(!stopped){
      everything_deactivated = false;
      error_message += error_message_process+"\n";
    }
  }
  return std::make_tuple(everything_deactivated, error_message);
}


std::tuple<bool, std::string> ResourceManager::stopProcesses(Capability capability)
{
  for(auto process : capability.getProcesses()){
    std::string process_path = "/"+drone_id_namespace+"/"+process+"/stop";

    ros::ServiceClient process_cli = node_handle.serviceClient<std_srvs::Empty>(process_path);

    if(!process_cli.call(empty_msg)){
      return std::make_tuple(false, "Process ["+process+"] could not be stopped");
    }
    std::cout << "[INFO]: Process: ["<< process << "] has been stopped" << std::endl;
  }
  return std::make_tuple(true, "");
}


std::tuple<bool, std::string> ResourceManager::stopUnreferenceCapabilities()
{
  std::vector<std::string> capabilities_deactivated;
  //int total_capabilities_stopped = 0;
  for(auto active_capability : active_capabilities){
    if(active_capability.second.getReferenceNumber() == 0){
      bool stopped; std::string error_message_processes;
      std::tie(stopped, error_message_processes) = stopProcesses(active_capability.second);
      if(!stopped){
        return std::make_tuple(false, error_message_processes);
      }

      capabilities_deactivated.push_back(active_capability.first);
    }
  }

  std::cout << "Stop unreference capabilities " << std::endl;
  for(auto active_capability : capabilities_deactivated){
    std::cout << "Capability: " << active_capability << std::endl;
    active_capabilities.erase(active_capability);
  }
  updateActiveResources();
  return std::make_tuple(true, "");
}


void ResourceManager::updateActiveResources()
{
  droneMsgsROS::ListOfCapabilities list_of_active_capabilities_msg;
  aerostack_msgs::ListOfProcesses list_of_active_processes_msg;

  std::for_each(active_capabilities.begin(), active_capabilities.end(),
		[&](std::pair<std::string, Capability> pair_capability)
		{
		  list_of_active_capabilities_msg.capabilities
		    .push_back(pair_capability.first);

		  for(auto process : pair_capability.second.getProcesses()){
		    list_of_active_processes_msg.processes.push_back(process);
		  }

		  /*list_of_active_processes_msg.active_processes
		    .reserve(pair_capability.second.getProcesses().size() +
			      pair_capability.second.getProcesses().size());*/

		  /*list_of_active_processes_msg.active_processes
		    .insert(list_of_active_processes_msg.active_processes.end(),
			    pair_capability.second.getProcesses().begin(),
			    pair_capability.second.getProcesses().end());*/

		});

  list_of_active_capabilities_topic.publish(list_of_active_capabilities_msg);
  list_of_active_processes_topic.publish(list_of_active_processes_msg);
}
