/*!*********************************************************************************
 *  \file       resource_manager_process_main.cpp
 *  \brief      ResourceManager main file.
 *  \details    This file implements the main function of the ResourceManager.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#include "../include/resource_manager_process.h"

int main(int argc, char** argv){
  ros::init(argc, argv, ros::this_node::getName());

  std::cout << ros::this_node::getName() << std::endl;

  ResourceManager resource_manager;
  resource_manager.setUp();

  try{
    resource_manager.start();
  }catch(std::exception &exception){
    resource_manager.notifyError(resource_manager.SafeguardRecoverableError, 0, "ownStart()", exception.what());
    resource_manager.stop();
  }


  ros::spin();
  return 0;
}
