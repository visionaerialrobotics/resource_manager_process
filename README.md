# Brief
This process is in charge of the activation of every process needed by the behaviors. This is useful for
optimizing the active processes.

# Services
- **activate_resources** ([droneMsgsROS/RequestResources](hhttps://bitbucket.org/joselusl/dronemsgsros/src/fa03af3fb09b943ea728d28683ff7b6032f74d66/srv/RequestResources.srv))  
Retrieves the capabilities associated to a behavior and, also, executes the processes associated to those capabilities retrieved.

- **cancel_resources** ([droneMsgsROS/RequestResources.srv](https://bitbucket.org/joselusl/dronemsgsros/src/fa03af3fb09b943ea728d28683ff7b6032f74d66/srv/RequestResources.srv))  
Retrieves the capabilities associated to a behavior and, also, stops the processes associated to those capabilities retrieved.

# Publish topics
- **list_of_active_capabilities** ([droneMsgsROS/ListOfCapabilities](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/msg/ListOfCapabilities.msg))  
Publishes a list of active capabilities every time a capability is activated or deactivated.

- **list_of_active_processes** ([aerostack_msgs/ListOfProcesses](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/msg/ListOfProcesses.msg))  
Publishes a list of active processes every time a process is activated or deactivated.

---
# Contributors
**Maintainer:** Alberto Camporredondo (alberto.camporredondo@gmail.com)  
**Author:** Alberto Camporredondo
